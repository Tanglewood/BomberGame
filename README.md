# Programming Test - Bomberman (Unreal) #

Unreal Engine 4 coding test. Time taken: ~12 hrs work over a 6 day period.

## Instructions ##

* Get Visual Studio 17
* Get Unreal Engine 4.18.2 from Epic Games Launcher
* Run Unreal Editor
* When propted browse for the project file BomberGame.uproject
* Hit Play in editor