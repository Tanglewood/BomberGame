// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BomberGameGameMode.h"
#include "BomberGamePlayerController.h"
#include "BomberGamePawn.h"

ABomberGameGameMode::ABomberGameGameMode()
{
	// no pawn by default
	DefaultPawnClass = ABomberGamePawn::StaticClass();
	// use our own player controller class
	PlayerControllerClass = ABomberGamePlayerController::StaticClass();
}
