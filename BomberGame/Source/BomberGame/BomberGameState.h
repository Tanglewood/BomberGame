// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Model/GameState.h"
#include "BomberGameState.generated.h"

/** GameMode class to specify pawn and playercontroller */
UCLASS(minimalapi)
class ABomberGameState : public AGameStateBase
{
	GENERATED_UCLASS_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, Instanced, Category = "BomberGame|GameState")
	UGameStateModel* GameStateModel;
};