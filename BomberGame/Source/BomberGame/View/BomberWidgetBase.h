// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#pragma once

#include "UserWidget.h"
#include "BomberWidgetBase.generated.h"

UCLASS(BlueprintType, Blueprintable)
class UBomberWidgetBase : public UUserWidget
{
	GENERATED_UCLASS_BODY()

protected:
};
