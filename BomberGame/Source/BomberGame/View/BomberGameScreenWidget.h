// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#pragma once

#include "BomberWidgetBase.h"
#include "BomberGameScreenWidget.generated.h"

UCLASS(BlueprintType, Blueprintable)
class UBomberGameScreenWidget : public UBomberWidgetBase
{
	GENERATED_UCLASS_BODY()

protected:
};
