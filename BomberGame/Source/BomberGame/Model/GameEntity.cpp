// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#include "GameEntity.h"

UGameEntity::UGameEntity(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

UBombEffect::UBombEffect(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

UBombPlayer::UBombPlayer(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

UBomb::UBomb(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}
UPowerup::UPowerup(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}
