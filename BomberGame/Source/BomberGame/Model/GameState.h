// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameBoard.h"
#include "GameEntity.h"
#include "GameState.generated.h"

namespace GameStateDefs
{
	const static float GlobalPlayerSpeedMod = 3.0f;
	const static float BombTimeoutSeconds = 2.5f;
	const static float BombCollisionPercent = 15.0f;	// amount which a player must protrude into an square to be hit by a bomb blast
	const static int PowerupAppearEvery = 3;
	const static float SpeedPowerupMultiplier = 1.25f;
	const static float GameTimeSeconds = 90.0f;
}

UENUM(BlueprintType)
enum class EGameResolution : uint8
{
	None,
	TimeUpDraw,
	BothDeadDraw,
	Player1Win,
	Player2Win
};

USTRUCT(BlueprintType)
struct FRenderItems
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "BomberGame|GameState")
	TArray<UGameEntity*> Entities;

	UPROPERTY(BlueprintReadWrite, Category = "BomberGame|GameState")
	TArray<UBombEffect*> Effects;
};

UCLASS(BlueprintType)
class UGameStateModel : public UObject
{
	GENERATED_UCLASS_BODY()

protected:
	UFUNCTION(BlueprintCallable, Category = "BomberGame|GameState")
	UGameBoard* GetBoard() { return CurrentBoard; }

	UFUNCTION(BlueprintCallable, Category = "BomberGame|GameState")
	void SetBoard(UGameBoard* InBoard) { InitialBoard = InBoard; }

	UFUNCTION(BlueprintCallable, Category = "BomberGame|GameState")
	void StartGame();

	UFUNCTION(BlueprintCallable, Category = "BomberGame|GameState")
	bool IsGameActive() const;

	UFUNCTION(BlueprintCallable, Category = "BomberGame|GameState")
	void SetPaused(bool bPaused);

	UFUNCTION(BlueprintCallable, Category = "BomberGame|GameState")
	bool IsPaused() const;

	UFUNCTION(BlueprintCallable, Category = "BomberGame|GameState")
	FRenderItems TickGame(float DeltaSeconds, const FBombPlayerInputStates& Player1Input, const FBombPlayerInputStates& Player2Input);

protected:
	UPROPERTY()
	UGameBoard* InitialBoard;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|GameState")
	UGameBoard* CurrentBoard;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|GameState")
	TArray<UGameEntity*> Entities;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|GameState")
	UBombPlayer* PlayerOneEntity;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|GameState")
	UBombPlayer* PlayerTwoEntity;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|GameState")
	float GameCountdownSeconds;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|GameState")
	EGameResolution GameResolution;

private:
	bool bIsActive;

	bool bIsPaused;

	int32 WinnerPlayerIndex = -1;

private:
	void MovePlayer(float DeltaSeconds, UBombPlayer* PlayerEntity, const FBombPlayerInputStates& PlayerInput);
	void DoPlayerBomb(float DeltaSeconds, UBombPlayer* PlayerEntity, const FBombPlayerInputStates& PlayerInput);
	void TickBombs(float DeltaSeconds, TArray<UBombEffect*>& OutEffects);
	void DoExplodeBomb(UBomb* InBomb, TArray<UBombEffect*>& OutEffects);
	UBombEffect* DoBreakBlock(int32 X, int32 Y);
	void KillPlayersInSquare(int32 X, int32 Y, TArray<UBombEffect*>& OutEffects);
	void TriggerBombsInSquare(int32 X, int32 Y, TArray<UBombEffect*>& OutEffects);
	void GeneratePowerup(int32 X, int32 Y);
	void GetPowerups();
	void ApplyPowerup(EPowerupType PowerupType, UBombPlayer* PlayerEntity);

	bool SquareBlocksPlayer(int32 X, int32 Y);

	bool SquareBlocksBlast(int32 X, int32 Y, bool& bIsBreakableBlock);

	bool CanBombSquare(int32 X, int32 Y);
};