// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#include "GameState.h"

UGameStateModel::UGameStateModel(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	InitialBoard = ObjectInitializer.CreateDefaultSubobject<UGameBoard>(this, FName("InitialBoard"));
	CurrentBoard = ObjectInitializer.CreateDefaultSubobject<UGameBoard>(this, FName("CurrentBoard"));
}

void UGameStateModel::StartGame()
{
	if (!bIsActive && InitialBoard != nullptr)
	{
		CurrentBoard->CopyStateFrom(InitialBoard);

		Entities.Empty();

		// Create players
		if (PlayerOneEntity == nullptr)
		{
			UBombPlayer* Player1 = NewObject<UBombPlayer>(this);
			PlayerOneEntity = Player1;
			Player1->PlayerIdx = 0;
			Player1->Score = 0;
		}

		int32 P1X, P1Y;
		CurrentBoard->GetPlayerStart1(P1X, P1Y);
		PlayerOneEntity->Location.X = P1X;
		PlayerOneEntity->Location.Y = P1Y;
		PlayerOneEntity->Speed = 1;
		PlayerOneEntity->BombsMax = 1;
		PlayerOneEntity->BombsAvailable = 1;
		PlayerOneEntity->BombLength = 1;
		PlayerOneEntity->RemoteBombTimeout = -1;
		PlayerOneEntity->bIsAlive = true;
		Entities.Add(PlayerOneEntity);

		if (PlayerTwoEntity == nullptr)
		{
			UBombPlayer* Player2 = NewObject<UBombPlayer>(this);
			PlayerTwoEntity = Player2;
			Player2->PlayerIdx = 1;
			Player2->Score = 0;
		}

		int32 P2X, P2Y;
		CurrentBoard->GetPlayerStart2(P2X, P2Y);
		PlayerTwoEntity->Location.X = P2X;
		PlayerTwoEntity->Location.Y = P2Y;
		PlayerTwoEntity->Speed = 1;
		PlayerTwoEntity->BombsMax = 1;
		PlayerTwoEntity->BombsAvailable = 1;
		PlayerTwoEntity->BombLength = 1;
		PlayerTwoEntity->RemoteBombTimeout = -1;
		PlayerTwoEntity->bIsAlive = true;
		Entities.Add(PlayerTwoEntity);

		WinnerPlayerIndex = -1;
		GameResolution = EGameResolution::None;
		GameCountdownSeconds = GameStateDefs::GameTimeSeconds;
		bIsActive = true;
	}
}

bool UGameStateModel::IsGameActive() const
{
	return bIsActive;
}

void UGameStateModel::SetPaused(bool bPaused)
{
	bIsPaused = bPaused;
}

bool UGameStateModel::IsPaused() const
{
	return bIsPaused;
}

FRenderItems UGameStateModel::TickGame(float DeltaSeconds, const FBombPlayerInputStates& Player1Input, const FBombPlayerInputStates& Player2Input)
{
	FRenderItems ReturnValue;

	if (!bIsActive) return ReturnValue;

	ReturnValue.Entities = Entities;
	if (bIsPaused || WinnerPlayerIndex >= 0) return ReturnValue;

	// Apply player move input
	MovePlayer(DeltaSeconds, PlayerOneEntity, Player1Input);
	MovePlayer(DeltaSeconds, PlayerTwoEntity, Player2Input);

	// Do bomb dropping
	DoPlayerBomb(DeltaSeconds, PlayerOneEntity, Player1Input);
	DoPlayerBomb(DeltaSeconds, PlayerTwoEntity, Player2Input);

	// Tick bombs
	TickBombs(DeltaSeconds, ReturnValue.Effects);

	// Pickup powerups
	GetPowerups();

	// Scores /end of the round?
	if (PlayerOneEntity->bIsAlive && !PlayerTwoEntity->bIsAlive)
	{
		bIsActive = false;
		PlayerOneEntity->Score += 1;
		WinnerPlayerIndex = 0;
		GameResolution = EGameResolution::Player1Win;
	}
	else if (!PlayerOneEntity->bIsAlive && PlayerTwoEntity->bIsAlive)
	{
		bIsActive = false;
		PlayerTwoEntity->Score += 1;
		WinnerPlayerIndex = 1;
		GameResolution = EGameResolution::Player2Win;
	}
	else if (!PlayerOneEntity->bIsAlive && !PlayerTwoEntity->bIsAlive)
	{
		// both players died in the same tick - draw?
		bIsActive = false;
		PlayerOneEntity->Score += 1;
		PlayerTwoEntity->Score += 1;
		WinnerPlayerIndex = -1;
		GameResolution = EGameResolution::BothDeadDraw;
	}

	// Countdown
	GameCountdownSeconds -= DeltaSeconds;
	if (GameCountdownSeconds <= 0.0f)
	{
		// Draw
		bIsActive = false;
		WinnerPlayerIndex = -1;
		GameResolution = EGameResolution::TimeUpDraw;
	}

	ReturnValue.Entities = Entities;
	return ReturnValue;
}

void UGameStateModel::MovePlayer(float DeltaSeconds, UBombPlayer* PlayerEntity, const FBombPlayerInputStates& PlayerInput)
{
	if (!PlayerEntity->bIsAlive) return;

	int RailX = FMath::FloorToInt(PlayerEntity->Location.X + 0.5f);
	int RailY = FMath::FloorToInt(PlayerEntity->Location.Y + 0.5f);

	float OffRailX = PlayerEntity->Location.X - (float)RailX;
	float OffRailY = PlayerEntity->Location.Y - (float)RailY;

	bool bOffRailX = OffRailX > 0.01f || OffRailX < -0.01f;
	bool bOffRailY = OffRailY > 0.01f || OffRailY < -0.01f;

	float ToMoveDistance = PlayerEntity->Speed * GameStateDefs::GlobalPlayerSpeedMod * DeltaSeconds;

	if (PlayerInput.XAxis > 0.5f)
	{
		// Try move right
		if (bOffRailY && !SquareBlocksPlayer(RailX + 1, RailY))
		{
			// Need to move vertically to rail in order to move right along rail
			if (OffRailY < 0.0f)
			{
				// Move down to match square alignment
				if (ToMoveDistance <= -OffRailY)
				{
					PlayerEntity->Location.Y += ToMoveDistance;
					ToMoveDistance = 0;
				}
				else
				{
					// We have enough to move onto the rail
					PlayerEntity->Location.Y = (float)RailY;
					ToMoveDistance += OffRailY;
					bOffRailY = false;
				}
			}
			else if (OffRailY > 0.0f)
			{
				// Move up to match square alignment
				if (ToMoveDistance <= OffRailY)
				{
					PlayerEntity->Location.Y -= ToMoveDistance;
					ToMoveDistance = 0;
				}
				else
				{
					// We have enough to move onto the rail
					PlayerEntity->Location.Y = (float)RailY;
					ToMoveDistance -= OffRailY;
					bOffRailY = false;
				}
			}
		}
		
		while (!bOffRailY && ToMoveDistance > 0.0f)
		{
			float DistanceToNextCollision = OffRailX > 0 ? 1.0f - OffRailX : -OffRailX;

			if (ToMoveDistance <= DistanceToNextCollision)
			{
				PlayerEntity->Location.X += ToMoveDistance;
				ToMoveDistance = 0;
			}
			else
			{
				PlayerEntity->Location.X += DistanceToNextCollision;
				ToMoveDistance -= DistanceToNextCollision;

				RailX = FMath::FloorToInt(PlayerEntity->Location.X + 0.5f);
				OffRailX = 0.0001f;	// small non-zero will get correct DistanceToNextCollision on next loop

				// Do collision with next square
				if (SquareBlocksPlayer(RailX + 1, RailY))
				{
					break; // can't move any farther
				}
			}
		}
	}
	else if(PlayerInput.XAxis < -0.5f)
	{
		// Try move left
		if (bOffRailY && !SquareBlocksPlayer(RailX - 1, RailY))
		{
			// Need to move vertically to rail in order to move left along rail
			if (OffRailY < 0.0f)
			{
				// Move down to match square alignment
				if (ToMoveDistance <= -OffRailY)
				{
					PlayerEntity->Location.Y += ToMoveDistance;
					ToMoveDistance = 0;
				}
				else
				{
					// We have enough to move onto the rail
					PlayerEntity->Location.Y = (float)RailY;
					ToMoveDistance += OffRailY;
					bOffRailY = false;
				}
			}
			else if (OffRailY > 0.0f)
			{
				// Move up to match square alignment
				if (ToMoveDistance <= OffRailY)
				{
					PlayerEntity->Location.Y -= ToMoveDistance;
					ToMoveDistance = 0;
				}
				else
				{
					// We have enough to move onto the rail
					PlayerEntity->Location.Y = (float)RailY;
					ToMoveDistance -= OffRailY;
					bOffRailY = false;
				}
			}
		}

		while (!bOffRailY && ToMoveDistance > 0.0f)
		{
			float DistanceToNextCollision = OffRailX >= 0 ? OffRailX : 1 + OffRailX;

			if (ToMoveDistance <= DistanceToNextCollision)
			{
				PlayerEntity->Location.X -= ToMoveDistance;
				ToMoveDistance = 0;
			}
			else
			{
				PlayerEntity->Location.X -= DistanceToNextCollision;
				ToMoveDistance -= DistanceToNextCollision;

				RailX = FMath::FloorToInt(PlayerEntity->Location.X + 0.5f);
				OffRailX = -0.0001f;	// small non-zero will get correct DistanceToNextCollision on next loop

				// Do collision with next square
				if (SquareBlocksPlayer(RailX - 1, RailY))
				{
					break; // can't move any farther
				}
			}
		}
	}
	else if (PlayerInput.YAxis > 0.5f)
	{
		// Try move down
		if (bOffRailX && !SquareBlocksPlayer(RailX, RailY + 1))
		{
			// Need to move horizontally to rail in order to move down along rail
			if (OffRailX < 0.0f)
			{
				// Move right to match square alignment
				if (ToMoveDistance <= -OffRailX)
				{
					PlayerEntity->Location.X += ToMoveDistance;
					ToMoveDistance = 0;
				}
				else
				{
					// We have enough to move onto the rail
					PlayerEntity->Location.X = (float)RailX;
					ToMoveDistance += OffRailX;
					bOffRailX = false;
				}
			}
			else if (OffRailX > 0.0f)
			{
				// Move left to match square alignment
				if (ToMoveDistance <= OffRailX)
				{
					PlayerEntity->Location.X -= ToMoveDistance;
					ToMoveDistance = 0;
				}
				else
				{
					// We have enough to move onto the rail
					PlayerEntity->Location.X = (float)RailX;
					ToMoveDistance -= OffRailX;
					bOffRailX = false;
				}
			}
		}

		while (!bOffRailX && ToMoveDistance > 0.0f)
		{
			float DistanceToNextCollision = OffRailY > 0 ? 1.0f - OffRailY : -OffRailY;

			if (ToMoveDistance <= DistanceToNextCollision)
			{
				PlayerEntity->Location.Y += ToMoveDistance;
				ToMoveDistance = 0;
			}
			else
			{
				PlayerEntity->Location.Y += DistanceToNextCollision;
				ToMoveDistance -= DistanceToNextCollision;

				RailY = FMath::FloorToInt(PlayerEntity->Location.Y + 0.5f);
				OffRailY = 0.0001f;	// small non-zero will get correct DistanceToNextCollision on next loop

				// Do collision with next square
				if (SquareBlocksPlayer(RailX, RailY + 1))
				{
					break; // can't move any farther
				}
			}
		}
	}
	else if (PlayerInput.YAxis < -0.5f)
	{
		// Try move up
		if (bOffRailX && !SquareBlocksPlayer(RailX, RailY - 1))
		{
			// Need to move horizontally to rail in order to move up along rail
			if (OffRailX < 0.0f)
			{
				// Move right to match square alignment
				if (ToMoveDistance <= -OffRailX)
				{
					PlayerEntity->Location.X += ToMoveDistance;
					ToMoveDistance = 0;
				}
				else
				{
					// We have enough to move onto the rail
					PlayerEntity->Location.X = (float)RailX;
					ToMoveDistance += OffRailX;
					bOffRailX = false;
				}
			}
			else if (OffRailX > 0.0f)
			{
				// Move left to match square alignment
				if (ToMoveDistance <= OffRailX)
				{
					PlayerEntity->Location.X -= ToMoveDistance;
					ToMoveDistance = 0;
				}
				else
				{
					// We have enough to move onto the rail
					PlayerEntity->Location.X = (float)RailX;
					ToMoveDistance -= OffRailX;
					bOffRailX = false;
				}
			}
		}

		while (!bOffRailX && ToMoveDistance > 0.0f)
		{
			float DistanceToNextCollision = OffRailY >= 0 ? OffRailY : 1 + OffRailY;

			if (ToMoveDistance <= DistanceToNextCollision)
			{
				PlayerEntity->Location.Y -= ToMoveDistance;
				ToMoveDistance = 0;
			}
			else
			{
				PlayerEntity->Location.Y -= DistanceToNextCollision;
				ToMoveDistance -= DistanceToNextCollision;

				RailY = FMath::FloorToInt(PlayerEntity->Location.Y + 0.5f);
				OffRailY = -0.0001f;	// small non-zero will get correct DistanceToNextCollision on next loop

				// Do collision with next square
				if (SquareBlocksPlayer(RailX, RailY - 1))
				{
					break; // can't move any farther
				}
			}
		}
	}

	PlayerEntity->LastInput = PlayerInput;
}

void UGameStateModel::DoPlayerBomb(float DeltaSeconds, UBombPlayer* PlayerEntity, const FBombPlayerInputStates& PlayerInput)
{
	if (PlayerEntity->bIsAlive && PlayerInput.bBomb && PlayerEntity->BombsAvailable > 0)
	{
		int X = FMath::FloorToInt(PlayerEntity->Location.X + 0.5f);
		int Y = FMath::FloorToInt(PlayerEntity->Location.Y + 0.5f);

		if (CanBombSquare(X, Y))
		{
			UBomb* NewBomb = NewObject<UBomb>(this);
			NewBomb->Location.X = X;
			NewBomb->Location.Y = Y;
			NewBomb->PlayerIdx = PlayerEntity->PlayerIdx;
			NewBomb->Timeout = GameStateDefs::BombTimeoutSeconds;
			NewBomb->BlastLength = PlayerEntity->BombLength;
			Entities.Add(NewBomb);

			PlayerEntity->BombsAvailable -= 1;
		}
	}
}

void UGameStateModel::TickBombs(float DeltaSeconds, TArray<UBombEffect*>& OutEffects)
{
	for (int32 i = Entities.Num() - 1; i >= 0; i--)
	{
		UGameEntity* Entity = Entities[i];
		UBomb* Bomb = Cast<UBomb>(Entity);
		if (Bomb && !Bomb->bExploded)
		{
			Bomb->Timeout -= DeltaSeconds;
			if (Bomb->Timeout <= 0)
			{
				// DoExplodeBomb can be re-entrant. marks bombs as exploded but don't remove them
				DoExplodeBomb(Bomb, OutEffects);
			}
		}
	}

	for (int32 i = Entities.Num() - 1; i >= 0; i--)
	{
		// Now remove exploded bombs
		UGameEntity* Entity = Entities[i];
		UBomb* Bomb = Cast<UBomb>(Entity);
		if (Bomb && Bomb->bExploded)
		{
			Entities.RemoveAt(i);
		}
	}
}

void UGameStateModel::DoExplodeBomb(UBomb* InBomb, TArray<UBombEffect*>& OutEffects)
{
	// Exploded bomb

	// Give bomb alloc back to player
	UBombPlayer* BombOwner = InBomb->PlayerIdx == 0 ? PlayerOneEntity : PlayerTwoEntity;
	BombOwner->BombsAvailable += 1;
	if (BombOwner->BombsAvailable > BombOwner->BombsMax)
	{
		BombOwner->BombsAvailable = BombOwner->BombsMax;
	}

	// Mark for removal
	InBomb->bExploded = true;

	// Create bomb explosion effects
	UBombEffect* Centre = NewObject<UBombEffect>(this);
	Centre->Location = InBomb->Location;
	Centre->EffectType = EBombEffect::BombBlastCentre;
	OutEffects.Add(Centre);

	// explosion: Go to right
	for (int32 r = 1; r <= InBomb->BlastLength; r++)
	{
		bool bIsBreakableBlock;
		if (SquareBlocksBlast(InBomb->Location.X + r, InBomb->Location.Y, bIsBreakableBlock))
		{
			if (bIsBreakableBlock)
			{
				OutEffects.Add(DoBreakBlock(InBomb->Location.X + r, InBomb->Location.Y));

				GeneratePowerup(InBomb->Location.X + r, InBomb->Location.Y);
			}
			break;
		}
		else
		{
			UBombEffect* Right = NewObject<UBombEffect>(this);
			Right->Location.X = InBomb->Location.X + r;
			Right->Location.Y = InBomb->Location.Y;
			Right->EffectType = r == InBomb->BlastLength ? EBombEffect::BombBlastRight : EBombEffect::BombBlastHoriz;
			OutEffects.Add(Right);

			KillPlayersInSquare(InBomb->Location.X + r, InBomb->Location.Y, OutEffects);
			TriggerBombsInSquare(InBomb->Location.X + r, InBomb->Location.Y, OutEffects);
		}
	}

	// explosion: Go to left
	for (int32 l = 1; l <= InBomb->BlastLength; l++)
	{
		bool bIsBreakableBlock;
		if (SquareBlocksBlast(InBomb->Location.X - l, InBomb->Location.Y, bIsBreakableBlock))
		{
			if (bIsBreakableBlock)
			{
				OutEffects.Add(DoBreakBlock(InBomb->Location.X - l, InBomb->Location.Y));
				// @todo: Chance of spawning a powerup
			}
			break;
		}
		else
		{
			UBombEffect* Left = NewObject<UBombEffect>(this);
			Left->Location.X = InBomb->Location.X - l;
			Left->Location.Y = InBomb->Location.Y;
			Left->EffectType = l == InBomb->BlastLength ? EBombEffect::BombBlastLeft : EBombEffect::BombBlastHoriz;
			OutEffects.Add(Left);

			KillPlayersInSquare(InBomb->Location.X - l, InBomb->Location.Y, OutEffects);
			TriggerBombsInSquare(InBomb->Location.X - l, InBomb->Location.Y, OutEffects);
		}
	}

	// explosion: Go down
	for (int32 d = 1; d <= InBomb->BlastLength; d++)
	{
		bool bIsBreakableBlock;
		if (SquareBlocksBlast(InBomb->Location.X, InBomb->Location.Y + d, bIsBreakableBlock))
		{
			if (bIsBreakableBlock)
			{
				OutEffects.Add(DoBreakBlock(InBomb->Location.X, InBomb->Location.Y + d));
				// @todo: Chance of spawning a powerup
			}
			break;
		}
		else
		{
			UBombEffect* Down = NewObject<UBombEffect>(this);
			Down->Location.X = InBomb->Location.X;
			Down->Location.Y = InBomb->Location.Y + d;
			Down->EffectType = d == InBomb->BlastLength ? EBombEffect::BombBlastBottom : EBombEffect::BombBlastVert;
			OutEffects.Add(Down);

			KillPlayersInSquare(InBomb->Location.X, InBomb->Location.Y + d, OutEffects);
			TriggerBombsInSquare(InBomb->Location.X, InBomb->Location.Y + d, OutEffects);
		}
	}

	// explosion: Go up
	for (int32 u = 1; u <= InBomb->BlastLength; u++)
	{
		bool bIsBreakableBlock;
		if (SquareBlocksBlast(InBomb->Location.X, InBomb->Location.Y - u, bIsBreakableBlock))
		{
			if (bIsBreakableBlock)
			{
				OutEffects.Add(DoBreakBlock(InBomb->Location.X, InBomb->Location.Y - u));
				// @todo: Chance of spawning a powerup
			}
			break;
		}
		else
		{
			UBombEffect* Up = NewObject<UBombEffect>(this);
			Up->Location.X = InBomb->Location.X;
			Up->Location.Y = InBomb->Location.Y - u;
			Up->EffectType = u == InBomb->BlastLength ? EBombEffect::BombBlastTop : EBombEffect::BombBlastVert;
			OutEffects.Add(Up);

			KillPlayersInSquare(InBomb->Location.X, InBomb->Location.Y - u, OutEffects);
			TriggerBombsInSquare(InBomb->Location.X, InBomb->Location.Y - u, OutEffects);
		}
	}
}

UBombEffect* UGameStateModel::DoBreakBlock(int32 X, int32 Y)
{
	UBombEffect* BreakBlockEffect = NewObject<UBombEffect>(this);
	BreakBlockEffect->Location.X = X;
	BreakBlockEffect->Location.Y = Y;
	BreakBlockEffect->EffectType = EBombEffect::BlockBreak;

	CurrentBoard->GetSquare(X, Y).Content = EBoardSquareContent::Empty;

	return BreakBlockEffect;
}

void UGameStateModel::KillPlayersInSquare(int32 X, int32 Y, TArray<UBombEffect*>& OutEffects)
{
	if (X < 0 || X >= GameBoardDefs::BoardSize) return;
	if (Y < 0 || Y >= GameBoardDefs::BoardSize) return;

	for (UGameEntity* Entity : Entities)
	{
		UBombPlayer* Player = Cast<UBombPlayer>(Entity);
		if (Player)
		{
			float ModFraction = GameStateDefs::BombCollisionPercent * 0.01f;

			// Play is "in" up to four squares - check them all
			int CheckSquareLeftX = FMath::FloorToInt(Player->Location.X + ModFraction);
			int CheckSquareTopY = FMath::FloorToInt(Player->Location.Y + ModFraction);
			int CheckSquareRightX = FMath::FloorToInt(Player->Location.X + 1.0f - ModFraction);
			int CheckSquareBottomY = FMath::FloorToInt(Player->Location.Y + 1.0f - ModFraction);

			if ((X == CheckSquareLeftX || X == CheckSquareRightX) && (Y == CheckSquareTopY || Y == CheckSquareBottomY))
			{
				// Kill player
				Player->bIsAlive = false;

				int PlayerSquareX = FMath::FloorToInt(Player->Location.X);
				int PlayerSquareY = FMath::FloorToInt(Player->Location.Y);

				UBombEffect* Death = NewObject<UBombEffect>(this);
				Death->Location.X = PlayerSquareX;
				Death->Location.Y = PlayerSquareY;
				Death->EffectType = EBombEffect::PlayerDeath;
				OutEffects.Add(Death);
			}
		}
	}
}

void UGameStateModel::TriggerBombsInSquare(int32 X, int32 Y, TArray<UBombEffect*>& OutEffects)
{
	if (X < 0 || X >= GameBoardDefs::BoardSize) return;
	if (Y < 0 || Y >= GameBoardDefs::BoardSize) return;

	for (UGameEntity* Entity : Entities)
	{
		UBomb* Bomb = Cast<UBomb>(Entity);
		if (Bomb && !Bomb->bExploded)
		{
			int BombSquareX = FMath::FloorToInt(Bomb->Location.X + 0.5f);
			int BombSquareY = FMath::FloorToInt(Bomb->Location.Y + 0.5f);

			if (X == BombSquareX && Y == BombSquareY)
			{
				DoExplodeBomb(Bomb, OutEffects);
			}
		}
	}
}

void UGameStateModel::GeneratePowerup(int32 X, int32 Y)
{
	if (FMath::Rand() % GameStateDefs::PowerupAppearEvery == 0)
	{
		UPowerup* NewPowerup = NewObject<UPowerup>(this);
		NewPowerup->Location.X = X;
		NewPowerup->Location.Y = Y;
		NewPowerup->Type = (EPowerupType)(FMath::Rand() % 3);
		Entities.Add(NewPowerup);
	}
}

void UGameStateModel::GetPowerups()
{
	int Player1X = FMath::FloorToInt(PlayerOneEntity->Location.X + 0.5f);
	int Player1Y = FMath::FloorToInt(PlayerOneEntity->Location.Y + 0.5f);

	int Player2X = FMath::FloorToInt(PlayerTwoEntity->Location.X + 0.5f);
	int Player2Y = FMath::FloorToInt(PlayerTwoEntity->Location.Y + 0.5f);

	for (int32 i = Entities.Num() - 1; i >= 0; i--)
	{
		UGameEntity* Entity = Entities[i];
		UPowerup* Powerup = Cast<UPowerup>(Entity);
		if (Powerup)
		{
			int PowerupX = FMath::FloorToInt(Powerup->Location.X + 0.5f);
			int PowerupY = FMath::FloorToInt(Powerup->Location.Y + 0.5f);

			if (PlayerOneEntity->bIsAlive && Player1X == PowerupX && Player1Y == PowerupY)
			{
				ApplyPowerup(Powerup->Type, PlayerOneEntity);
				Entities.RemoveAt(i);
			}
			else if (PlayerTwoEntity->bIsAlive && Player2X == PowerupX && Player2Y == PowerupY)
			{
				ApplyPowerup(Powerup->Type, PlayerTwoEntity);
				Entities.RemoveAt(i);
			}
		}
	}
}

void UGameStateModel::ApplyPowerup(EPowerupType PowerupType, UBombPlayer* PlayerEntity)
{
	switch (PowerupType)
	{
	case EPowerupType::Speed:
		PlayerEntity->Speed *= GameStateDefs::SpeedPowerupMultiplier;
		break;
	case EPowerupType::ExtraBomb:
		PlayerEntity->BombsMax += 1;
		PlayerEntity->BombsAvailable += 1;
		break;
	case EPowerupType::LongerBomb:
		PlayerEntity->BombLength += 1;
		break;
	}
}

bool UGameStateModel::SquareBlocksPlayer(int32 X, int32 Y)
{
	if (X < 0 || X >= GameBoardDefs::BoardSize) return true;
	if (Y < 0 || Y >= GameBoardDefs::BoardSize) return true;

	const FGameBoardSquare& Square = CurrentBoard->GetSquare(X, Y);

	return Square.Content == EBoardSquareContent::BreakableBlock || Square.Content == EBoardSquareContent::UnbreakableBlock;
}

bool UGameStateModel::SquareBlocksBlast(int32 X, int32 Y, bool& bIsBreakableBlock)
{
	bIsBreakableBlock = false;

	if (X < 0 || X >= GameBoardDefs::BoardSize) return true;
	if (Y < 0 || Y >= GameBoardDefs::BoardSize) return true;

	const FGameBoardSquare& Square = CurrentBoard->GetSquare(X, Y);

	bIsBreakableBlock = Square.Content == EBoardSquareContent::BreakableBlock;
	return bIsBreakableBlock || Square.Content == EBoardSquareContent::UnbreakableBlock;
}

bool UGameStateModel::CanBombSquare(int32 X, int32 Y)
{
	if (X < 0 || X >= GameBoardDefs::BoardSize) return false;
	if (Y < 0 || Y >= GameBoardDefs::BoardSize) return false;

	for (UGameEntity* Entity : Entities)
	{
		UBomb* Bomb = Cast<UBomb>(Entity);
		if (Bomb)
		{
			int BombX = FMath::FloorToInt(Bomb->Location.X + 0.5f);
			int BombY = FMath::FloorToInt(Bomb->Location.Y + 0.5f);

			if (BombX == X && BombY == Y) return false;		// already a bomb here
		}
	}

	return true;
}