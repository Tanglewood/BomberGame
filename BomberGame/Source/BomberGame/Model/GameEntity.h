// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameEntity.generated.h"

UCLASS(BlueprintType)
class UGameEntity : public UObject
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	FVector2D Location;
};

UENUM(BlueprintType)
enum class EBombEffect : uint8
{
	BombBlastCentre,
	BombBlastHoriz,
	BombBlastVert,
	BombBlastRight,
	BombBlastLeft,
	BombBlastBottom,
	BombBlastTop,
	BlockBreak,
	PlayerDeath
};

UCLASS(BlueprintType)
class UBombEffect : public UGameEntity
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Effect")
	EBombEffect EffectType;
};

USTRUCT(BlueprintType)
struct FBombPlayerInputStates
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite, Category = "BomberGame|Entity")
	int32 XAxis;

	UPROPERTY(BlueprintReadWrite, Category = "BomberGame|Entity")
	int32 YAxis;

	UPROPERTY(BlueprintReadWrite, Category = "BomberGame|Entity")
	bool bBomb;
};

UCLASS(BlueprintType)
class UBombPlayer : public UGameEntity
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	int32 PlayerIdx;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	int32 Score;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	float Speed;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	int32 BombsMax;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	int32 BombsAvailable;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	int32 BombLength;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	int32 RemoteBombTimeout;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	bool bIsAlive;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	FBombPlayerInputStates LastInput;
};

UCLASS(BlueprintType)
class UBomb : public UGameEntity
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	int32 PlayerIdx;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	float Timeout;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	int32 BlastLength;

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	bool bExploded = false;
};

UENUM(BlueprintType)
enum class EPowerupType : uint8
{
	Speed,
	ExtraBomb,
	LongerBomb,
	//RemoteControl		@todo
};

UCLASS(BlueprintType)
class UPowerup : public UGameEntity
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|Entity")
	EPowerupType Type;
};

