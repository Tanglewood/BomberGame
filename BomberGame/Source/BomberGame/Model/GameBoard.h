// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameBoard.generated.h"

namespace GameBoardDefs
{
	const static int32 BoardSize = 13;
}

UENUM(BlueprintType)
enum class EBoardSquareContent : uint8
{
	Empty,
	UnbreakableBlock,
	BreakableBlock,
	PlayerStart1,
	PlayerStart2
};

USTRUCT(BlueprintType)
struct FGameBoardSquare
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadOnly, Category = "BomberGame|GameBoard")
	EBoardSquareContent Content;
};

UCLASS(BlueprintType)
class UGameBoard : public UObject
{
	GENERATED_UCLASS_BODY()

public:
	UPROPERTY()
	FGameBoardSquare Squares[GameBoardDefs::BoardSize*GameBoardDefs::BoardSize];

public:
	void CopyStateFrom(const UGameBoard* InBoard);

	void GetPlayerStart1(int32& OutX, int32& OutY) const;

	void GetPlayerStart2(int32& OutX, int32& OutY) const;

public:
	UFUNCTION(BlueprintCallable, Category="BomberGame|GameBoard")
	int GetBoardSize() const { return GameBoardDefs::BoardSize; }

	UFUNCTION(BlueprintCallable, Category = "BomberGame|GameBoard")
	const FGameBoardSquare& GetSquare(int32 X, int32 Y) const;

	FGameBoardSquare& GetSquare(int32 X, int32 Y);
};