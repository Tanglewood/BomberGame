// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#include "GameBoard.h"

UGameBoard::UGameBoard(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	check(GameBoardDefs::BoardSize == 13);

	GetSquare(0, 0).Content = EBoardSquareContent::PlayerStart1;
	GetSquare(1, 0).Content = EBoardSquareContent::Empty;
	GetSquare(2, 0).Content = EBoardSquareContent::Empty;
	GetSquare(3, 0).Content = EBoardSquareContent::Empty;
	GetSquare(4, 0).Content = EBoardSquareContent::Empty;
	GetSquare(5, 0).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(6, 0).Content = EBoardSquareContent::Empty;
	GetSquare(7, 0).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(8, 0).Content = EBoardSquareContent::Empty;
	GetSquare(9, 0).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(10, 0).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(11, 0).Content = EBoardSquareContent::Empty;
	GetSquare(12, 0).Content = EBoardSquareContent::BreakableBlock;

	GetSquare(0, 1).Content = EBoardSquareContent::Empty;
	GetSquare(1, 1).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(2, 1).Content = EBoardSquareContent::Empty;
	GetSquare(3, 1).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(4, 1).Content = EBoardSquareContent::Empty;
	GetSquare(5, 1).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(6, 1).Content = EBoardSquareContent::Empty;
	GetSquare(7, 1).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(8, 1).Content = EBoardSquareContent::Empty;
	GetSquare(9, 1).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(10, 1).Content = EBoardSquareContent::Empty;
	GetSquare(11, 1).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(12, 1).Content = EBoardSquareContent::Empty;

	GetSquare(0, 2).Content = EBoardSquareContent::PlayerStart1;
	GetSquare(1, 2).Content = EBoardSquareContent::Empty;
	GetSquare(2, 2).Content = EBoardSquareContent::Empty;
	GetSquare(3, 2).Content = EBoardSquareContent::Empty;
	GetSquare(4, 2).Content = EBoardSquareContent::Empty;
	GetSquare(5, 2).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(6, 2).Content = EBoardSquareContent::Empty;
	GetSquare(7, 2).Content = EBoardSquareContent::Empty;
	GetSquare(8, 2).Content = EBoardSquareContent::Empty;
	GetSquare(9, 2).Content = EBoardSquareContent::Empty;
	GetSquare(10, 2).Content = EBoardSquareContent::Empty;
	GetSquare(11, 2).Content = EBoardSquareContent::Empty;
	GetSquare(12, 2).Content = EBoardSquareContent::BreakableBlock;

	GetSquare(0, 3).Content = EBoardSquareContent::Empty;
	GetSquare(1, 3).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(2, 3).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(3, 3).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(4, 3).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(5, 3).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(6, 3).Content = EBoardSquareContent::Empty;
	GetSquare(7, 3).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(8, 3).Content = EBoardSquareContent::Empty;
	GetSquare(9, 3).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(10, 3).Content = EBoardSquareContent::Empty;
	GetSquare(11, 3).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(12, 3).Content = EBoardSquareContent::Empty;

	GetSquare(0, 4).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(1, 4).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(2, 4).Content = EBoardSquareContent::Empty;
	GetSquare(3, 4).Content = EBoardSquareContent::Empty;
	GetSquare(4, 4).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(5, 4).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(6, 4).Content = EBoardSquareContent::Empty;
	GetSquare(7, 4).Content = EBoardSquareContent::Empty;
	GetSquare(8, 4).Content = EBoardSquareContent::Empty;
	GetSquare(9, 4).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(10, 4).Content = EBoardSquareContent::Empty;
	GetSquare(11, 4).Content = EBoardSquareContent::Empty;
	GetSquare(12, 4).Content = EBoardSquareContent::Empty;

	GetSquare(0, 5).Content = EBoardSquareContent::Empty;
	GetSquare(1, 5).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(2, 5).Content = EBoardSquareContent::Empty;
	GetSquare(3, 5).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(4, 5).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(5, 5).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(6, 5).Content = EBoardSquareContent::Empty;
	GetSquare(7, 5).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(8, 5).Content = EBoardSquareContent::Empty;
	GetSquare(9, 5).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(10, 5).Content = EBoardSquareContent::Empty;
	GetSquare(11, 5).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(12, 5).Content = EBoardSquareContent::Empty;

	GetSquare(0, 6).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(1, 6).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(2, 6).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(3, 6).Content = EBoardSquareContent::Empty;
	GetSquare(4, 6).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(5, 6).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(6, 6).Content = EBoardSquareContent::Empty;
	GetSquare(7, 6).Content = EBoardSquareContent::Empty;
	GetSquare(8, 6).Content = EBoardSquareContent::Empty;
	GetSquare(9, 6).Content = EBoardSquareContent::Empty;
	GetSquare(10, 6).Content = EBoardSquareContent::Empty;
	GetSquare(11, 6).Content = EBoardSquareContent::Empty;
	GetSquare(12, 6).Content = EBoardSquareContent::Empty;

	GetSquare(0, 7).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(1, 7).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(2, 7).Content = EBoardSquareContent::Empty;
	GetSquare(3, 7).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(4, 7).Content = EBoardSquareContent::Empty;
	GetSquare(5, 7).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(6, 7).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(7, 7).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(8, 7).Content = EBoardSquareContent::Empty;
	GetSquare(9, 7).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(10, 7).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(11, 7).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(12, 7).Content = EBoardSquareContent::Empty;

	GetSquare(0, 8).Content = EBoardSquareContent::Empty;
	GetSquare(1, 8).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(2, 8).Content = EBoardSquareContent::Empty;
	GetSquare(3, 8).Content = EBoardSquareContent::Empty;
	GetSquare(4, 8).Content = EBoardSquareContent::Empty;
	GetSquare(5, 8).Content = EBoardSquareContent::Empty;
	GetSquare(6, 8).Content = EBoardSquareContent::Empty;
	GetSquare(7, 8).Content = EBoardSquareContent::Empty;
	GetSquare(8, 8).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(9, 8).Content = EBoardSquareContent::Empty;
	GetSquare(10, 8).Content = EBoardSquareContent::Empty;
	GetSquare(11, 8).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(12, 8).Content = EBoardSquareContent::BreakableBlock;

	GetSquare(0, 9).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(1, 9).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(2, 9).Content = EBoardSquareContent::Empty;
	GetSquare(3, 9).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(4, 9).Content = EBoardSquareContent::Empty;
	GetSquare(5, 9).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(6, 9).Content = EBoardSquareContent::Empty;
	GetSquare(7, 9).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(8, 9).Content = EBoardSquareContent::Empty;
	GetSquare(9, 9).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(10, 9).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(11, 9).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(12, 9).Content = EBoardSquareContent::Empty;

	GetSquare(0, 10).Content = EBoardSquareContent::Empty;
	GetSquare(1, 10).Content = EBoardSquareContent::Empty;
	GetSquare(2, 10).Content = EBoardSquareContent::Empty;
	GetSquare(3, 10).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(4, 10).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(5, 10).Content = EBoardSquareContent::Empty;
	GetSquare(6, 10).Content = EBoardSquareContent::Empty;
	GetSquare(7, 10).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(8, 10).Content = EBoardSquareContent::Empty;
	GetSquare(9, 10).Content = EBoardSquareContent::Empty;
	GetSquare(10, 10).Content = EBoardSquareContent::Empty;
	GetSquare(11, 10).Content = EBoardSquareContent::Empty;
	GetSquare(12, 10).Content = EBoardSquareContent::PlayerStart2;

	GetSquare(0, 11).Content = EBoardSquareContent::Empty;
	GetSquare(1, 11).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(2, 11).Content = EBoardSquareContent::Empty;
	GetSquare(3, 11).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(4, 11).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(5, 11).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(6, 11).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(7, 11).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(8, 11).Content = EBoardSquareContent::Empty;
	GetSquare(9, 11).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(10, 11).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(11, 11).Content = EBoardSquareContent::UnbreakableBlock;
	GetSquare(12, 11).Content = EBoardSquareContent::Empty;

	GetSquare(0, 12).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(1, 12).Content = EBoardSquareContent::Empty;
	GetSquare(2, 12).Content = EBoardSquareContent::Empty;
	GetSquare(3, 12).Content = EBoardSquareContent::Empty;
	GetSquare(4, 12).Content = EBoardSquareContent::Empty;
	GetSquare(5, 12).Content = EBoardSquareContent::Empty;
	GetSquare(6, 12).Content = EBoardSquareContent::Empty;
	GetSquare(7, 12).Content = EBoardSquareContent::Empty;
	GetSquare(8, 12).Content = EBoardSquareContent::Empty;
	GetSquare(9, 12).Content = EBoardSquareContent::Empty;
	GetSquare(10, 12).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(11, 12).Content = EBoardSquareContent::BreakableBlock;
	GetSquare(12, 12).Content = EBoardSquareContent::BreakableBlock;
}

void UGameBoard::CopyStateFrom(const UGameBoard* InBoard)
{
	for (int32 i = 0; i < GameBoardDefs::BoardSize*GameBoardDefs::BoardSize; i++)
	{
		Squares[i] = InBoard->Squares[i];
	}
}

void UGameBoard::GetPlayerStart1(int32& OutX, int32& OutY) const
{
	for (int32 i = 0; i < GameBoardDefs::BoardSize*GameBoardDefs::BoardSize; i++)
	{
		if (Squares[i].Content == EBoardSquareContent::PlayerStart1)
		{
			OutX = i % GameBoardDefs::BoardSize;
			OutY = i / GameBoardDefs::BoardSize;
			return;
		}
	}
}

void UGameBoard::GetPlayerStart2(int32& OutX, int32& OutY) const
{
	for (int32 i = 0; i < GameBoardDefs::BoardSize*GameBoardDefs::BoardSize; i++)
	{
		if (Squares[i].Content == EBoardSquareContent::PlayerStart2)
		{
			OutX = i % GameBoardDefs::BoardSize;
			OutY = i / GameBoardDefs::BoardSize;
			return;
		}
	}
}

const FGameBoardSquare& UGameBoard::GetSquare(int32 X, int32 Y) const
{
	check(X > -1 && X < GameBoardDefs::BoardSize);
	check(Y > -1 && Y < GameBoardDefs::BoardSize);

	return Squares[Y*GameBoardDefs::BoardSize + X];
}

FGameBoardSquare& UGameBoard::GetSquare(int32 X, int32 Y)
{
	check(X > -1 && X < GameBoardDefs::BoardSize);
	check(Y > -1 && Y < GameBoardDefs::BoardSize);

	return Squares[Y*GameBoardDefs::BoardSize + X];
}
