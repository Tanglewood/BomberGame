// Copyright 2018 Tanglewood Games Limited. All Rights Reserved.

#include "BomberGameState.h"

ABomberGameState::ABomberGameState(const class FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	GameStateModel = ObjectInitializer.CreateDefaultSubobject<UGameStateModel>(this, FName("GameStateModel"));
}