// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "BomberGamePlayerController.h"

ABomberGamePlayerController::ABomberGamePlayerController()
{
	bShowMouseCursor = true;
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
}
